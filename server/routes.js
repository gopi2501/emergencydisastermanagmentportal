var eocmodel = require('../models/eocmodel');
var disastermodel = require('../models/disastermodel');
var express = require('express')
var router = express.Router();
var md5 = require('md5');

var currentuser;
// var nodemailer = require('nodemailer');
// module.exports = function (app) {
router.get('/', function (req, res) {
    console.log('in get / ');
    // app.get('/', function (req, res) {
    res.render('login', {
        warningmessage1: "",
        warningmessageerror : ""
    });

});

router.post('/', function (req, res) {
    console.log('post of login page');
    var email = req.body.email;
    console.log('email entered', email);
    currentuser = eocmodel.find({ email: email }, function (err, docs) {
        if (err) { console.log(err) } else {
            console.log(docs);
        }
    });
    console.log('password before hashing ', req.body.password);
    var password = md5(req.body.password);
    
    
    console.log('email', email + 'password', password);
   
    eocmodel.find({ email: email, password: password }, function (err, docs) {
        console.log('right after eocmodel finding for login');
        if (err) {
            console.log('issue while logging in', err);
        }
        if (!docs.length) {
            console.log('username obtained is' + docs.username);
            res.render('login', {
                warningmessageerror: "invalid email or password !!",
                warningmessage1  : ""
            });
            // window.alert('Username does not exists');
            console.log('username does not exists');
        } else {
            console.log('user exists', docs);
            //    res.send(docs);
            if (docs.length > 0) {
                console.log('the lastname is ', docs[0].lastname);
                res.render('eocdashboard', {
                    myname: docs[0].firstname
                });
            }
        }
    });
});

router.get('/eocregistration', function (req, res) {
    // app.get('/studentregister', function(req,res){
    // document.getElementById('username').innerHTML=currentuser;
    res.render('eocregistration');
});

router.post('/eocregistration', function (req, res) {
    var firstname = req.body.firstname;
    var lastname = req.body.lastname;
    var homeaddress = req.body.homeaddress;
    var homecity = req.body.homecity;
    var homestate = req.body.homestate;
    var homezip = req.body.homezip;
    var officeaddress = req.body.officeaddress;
    var officecity = req.body.officecity;
    var officestate = req.body.officestate;
    var officezip = req.body.officezip;
    var title = req.body.title;
    var mobilenumber = req.body.mobilenumber;
    var homenumber = req.body.homenumber;
    var officenumber = req.body.officenumber;
    var email = req.body.email;
    var password = md5(req.body.password);
    // var checks = req.body.getElementsByClassName('certificationchecks');
    // var listofcert = "";
    // for (var init = 0; init < 9; init++) {
    //     if (checks[init].checked === true) {
    //         listofcert += checks[init].value + ',';
    //     }
    // }
   
    var neweocmember = new eocmodel({
        firstname: firstname, lastname: lastname,
        homeaddress: {
            address: homeaddress, city: homecity, state: homestate, zip: homezip
        },
        officeAddress: {
            address: officeaddress, city: officecity, state: officestate, zip: officezip
        },
        title: title,
        phonenumber: { mobile: mobilenumber, home: homenumber, office: officenumber },
        email: email, password: password,
        // certifications: listofcert
    });
    console.log('in routes.js before calling create user');

    neweocmember.save(function (err, result) {
        if (err) console.log(err);
        else {
            console.log('successfully inserted an eoc members record');
            // res.send('user added successfully');
            res.render('login', {
                warningmessage1: "Registration Successful. Please login",
                warningmessageerror: ""
            });
        }
    });
});

// router.get('/registeradisaster', function (req, res) {
//     // var disasterdata = setTimeout(disasterpopulater, 5000);
//    var disasterdata=disasterpopulater();
//     console.log("after data from disaster populater "+disasterdata);
//     // res.send(CreateTableFromJSON(disasterdata));
//     res.render('registeradisaster', {
//         registerationmessage: "",
//         disasterdata:disasterdata
//         // CreateTableFromJSON(disasterdata)
//     });
// });

//useful link: https://stackoverflow.com/questions/24592760/how-can-i-pass-an-array-to-an-ejs-template-in-express

router.get('/registeradisaster', function (req, res) {
    disastermodel.find({}, function (err, docs) {
        if (err) {
            console.log('no registered disaster');
        } else {
            console.log('before redirecting to registeradisaster', docs);
            res.render('registeradisaster', {
                registerationmessage: " sdf",
                disasterdata: docs,
                disastertext: docs,
                errormessage: "",
                tablefunction: CreateTableFromJSON,
                message: ""
            });
        }
    });
});
// function disasterpopulater() {
//     var alldisasters=new Array();
//     console.log("call for disaster populater");
//     // var alldisasters = disastermodel.find({contactperson:"Aswini"});
//     // disastermodel.find({ contactperson: "Aswini" }, function (err, docs) {
//     disastermodel.find({}, function (err, docs) {
//         if (!docs.length) {
//             console.log('no disasters registerd');

//         } else {
//             alldisasters = docs;
//             console.log('printing all disasters in disasterpopulater', alldisasters);
//             // CreateTableFromJSON();
//             // console.log(docs);
//             return alldisasters;
//         }
//     })

// }



router.post('/registeradisaster', function (req, res) {
    var dropdown = req.body.disasterdropdown;
    console.log('disasterdropdownvalue is', dropdown);
    var disastername = req.body.disastername;
    var contactperson = req.body.contactperson;
    var disasteraddress = req.body.disasteraddress;
    var disastercity = req.body.disastercity;
    var state = req.body.state;
    var zipcode = req.body.zipcode;
    var contactperson = req.body.contactperson;
    var dateandtime = req.body.dateandtime;
    var newdisaster = new disastermodel({
        disastername: disastername,
        disasteraddress: disasteraddress,
        disastercity: disastercity,
        disasterdropdown: dropdown,
        contactperson: contactperson,
        numberofimages: 0,
        state: state,
        zipcode: zipcode,
        dateandtime: dateandtime
    });


    newdisaster.save(function (err, result) {
        if (err) {
            console.log(err);
            res.render('registeradisaster', {
                registerationmessage: "registration failed",
                errormessage: "",
                message: ""
            });
        } else {
            // res.redirect('/registeradisaster');
            disastermodel.find({}, function (err, docs) {
                if (err) {
                    console.log('no registered disaster');
                } else {
                    console.log('before redirecting to registeradisaster', docs);
                    res.render('registeradisaster', {
                        registerationmessage: " sdf",
                        disasterdata: docs,
                        disastertext: docs,
                        errormessage: "",
                        tablefunction: CreateTableFromJSON,
                        message: "Disaster added successfully"
                    });
                }
            });
        }
    })
})

router.post('/approveacceptedid', function (req, res) {
    var checkedvalue = req.body.acceptedid;
    console.log('------checked value: ', checkedvalue);
})

router.get('/forgotpassword', function (req, res) {
    res.render('forgot', {
        messageforemail: ""
    });
});

router.post('/emailforgotpassword', function (req, res) {
    var toemail = req.body.email;
    console.log('forgot password email request has been received');
    console.log(toemail);
    var nodemailer = require('nodemailer');

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'lgopisaikrishna@gmail.com',
            pass: '9394035748'
        }
    });

    console.log('done with transported');
    var mailOptions = {
        from: 'lgopisaikrishna@gmail.com',
        to: toemail,
        subject: 'Forgot Password EOC Personnal',
        text: 'Click this URL to reset your password (https://www.nowwhere.com/)'
      };

      console.log('mail options done with it');
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log('error has occured');
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
     

    res.render('forgot', {
        messageforemail: "Email sent successfuly"
    })
});



// router.get('/emailforgotpassword', function (req, res) {
//     var email=req.body.email;
//     var nodemailer = require('nodemailer');
//     var transporter = nodemailer.createTransport({
//         service: 'gmail',
//         auth: {
//             user: 'lgopisaikrishna@gmail.com',
//             pass: '9394035748'
//         }
//     });

//     console.log('on creating nodemailer createTransport');
//     const mailOptions = {
//         from: 'lgopisaikrishna@gmail.com', // sender address
//         to: email, // list of receivers
//         subject: 'Forgot Password for EOC.', // Subject line
//         //html: firstname + ' ' + lastname + ' says: ' + message,// plain text body
//         html: 'Please click on this link to reset your password.'
//     };
//     console.log('mail options');
//     transporter.sendMail(mailOptions, function (err, info) {
//         if (err)
//             console.log(err)
//         else
//             console.log(info);
//     });
//     res.render('contact', {
//         message: "mail sent succesfully"
//     });
// });

//});

router.get('/eocdashboard', function (req, res) {
    res.render('eocdashboard', {
        myname: ""
    });
});

router.get('/blanktemplate', function (req, res) {
    res.render('blank', {
        registerationmessage: " sdf",
        disasterdata: "",
        disastertext: "",
        errormessage: "",
        tablefunction: ""
    });
});

router.get('/viewalldisasters', function (req, res) {
    // res.render('viewalldisasters');
    disastermodel.find({}, function (err, docs) {
        if (err) {
            console.log('no registered disaster');
        } else {
            // console.log('before redirecting to registeradisaster', docs);
            res.render('viewalldisasters', {
                registerationmessage: " sdf",
                disasterdata: docs,
                disastertext: docs,
                errormessage: "",
                tablefunction: CreateTableFromJSON
            });
        }
    });
});
router.get('/generatereports', function (req, res) {
    // res.render('viewalldisasters');
    disastermodel.find({}, function (err, docs) {
        if (err) {
            console.log('no registered disaster');
        } else {
            console.log('before redirecting to registeradisaster', docs);
            res.render('generatereports', {
                registerationmessage: " sdf",
                disasterdata: docs,
                disastertext: docs,
                errormessage: "",
                tablefunction: CreateTableFromJSON
            });
        }
    });
});

router.get('/dashboard', function (req, res) {
    res.render('dashboard', {
        myname: currentuser
    });
});

router.get('/login', function (req, res) {
    res.render('login', {
        warningmessage1: "",
        warningmessageerror : ""

    });
});

router.get('/trialpage', function (req, res) {

    res.render('trialpage');

});

router.get('/trialtable', function (req, res) {
    res.render('trialtables');
});

router.post('/editadisaster/:disastername/:dateandtime', function (req, res) {
    var disastername = req.params.disastername;
    var dateandtime = req.params.dateandtime;

    var contactperson = req.body.contactperson;
    var disasteraddress = req.body.disasteraddress;
    var state = req.body.state;
    var zip = req.body.zip;
    console.log('disastername is ', disastername);
    console.log('disaster date and time is ', dateandtime);
    disastermodel.findOne({ disastername: disastername, dateandtime: dateandtime }, function (err, docs) {

        var alldata = docs;
        console.log('all data of the disaster', alldata);
        if (err) {
            res.render('disasterpage', {
                messagefromdb: "could not find the disaster",
                docs: alldata
            });
        } else {
            console.log('found the disaster and i am ready to edit the disaster');
            var query = { disastername: disastername, dateandtime: dateandtime };
            var updatedvalues = { contactperson: contactperson, disasteraddress: disasteraddress, state: state, zip: zip };

            disastermodel.updateOne(query, updatedvalues, function (err, updatedrecord) {

                console.log('just finished the query to update data');
                if (err) {
                    console.log('error in updating the record');
                    res.render('disasterpage', {
                        messagefromdb: "failed to udpate the record",
                        docs: alldata
                    });
                } else {
                    
                    disastermodel.findOne({ disastername: disastername }, function (err, docs) {

                        if (err) {
                            console.log('issue while retrieving');
                            res.render('disasterpage', {
                                messagefromdb: "failed to get the record",
                                docs
                            });
                        } else {
                           
                            console.log('docs on after updating', docs);
                            
                            var tourl = '/disasterpage_' + disastername;
                            res.redirect(tourl);
                            messagefromdb: "Disaster details updated successfully";
                        }
                    });
                }
            });
        }
    });
});
// $('.confirm').on('click', function() {
//     $.alertable.confirm('You sure?').then(function() {
//       console.log('Confirmation submitted');
//     }, function() {
//       console.log('Confirmation canceled');      
//     });
//   });

router.get('/deleteadisaster_:disastername', function (req, res) {
    var disastername = req.params.disastername;
    console.log('Deleting the disaster', disastername);
    disastermodel.findOne({ disastername: disastername }, function (err, docs) {
        if (err) {
            res.render('viewalldisasters', {
                errormessage: "Error while finding the disaster in database"
            });
        } else {
        
            console.log('found the disaster before deleting the disaster');
            //write the delete query

            // disastermodel.deleteOne({disastername: disastername});
            // alert("Do you want to delete a disaster?");
            
            disastermodel.remove({ disastername: disastername }, function (err, obj) {
                if (err) { throw err; }
                else {
                    disastermodel.find({ disastername: disastername }, function (err, docs) {
                        if (err) {
                            console.log('no registered disaster');
                        } else {
                            console.log('before redirecting to registeradisaster', docs);
                           
                            // res.render('viewalldisasters', {
                            //     registerationmessage: " sdf",
                            //     disasterdata: docs,
                            //     disastertext: docs,
                            //     errormessage: "",
                            //     tablefunction: CreateTableFromJSON
                            // });
                            res.redirect('/viewalldisasters');
                        }
                    });
                }

            });
        }
    });
});
router.get('/disasterpage_:disastername', function (req, res) {

    var disastername = req.params.disastername;
    disastermodel.findOne({ disastername: disastername }, function (err, docs) {
        if (err) {
            res.render('viewalldisasters', {
                errormessage: "Error while extracting from the database"
            });
        } else {
            console.log('priniting details before redirecting to disaster page');
            console.log(docs);
            // var docs=(docs);
            res.render('disasterpage', {
                docs: docs,
                address: docs.disasteraddress,
                messagefromdb: ""
            });
        }
    });
});

router.get('/disasterpagee', function (req, res) {
    res.render('disasterpage', {
        messagefromdb: ""
    });
});


router.get('/chatbox', function (req, res) {
    res.render('chat');
});

router.get('/getacceptedid/:disastername', function (req, res) {
    var disastername = req.params.disastername;
    console.log('disaster copied is ', disastername);
    disastermodel.findOne({ disastername: disastername }, function (err, docs) {
        if (err) {
            console.log(err);
        } else {
            console.log('after getaccepted id');
            console.log(docs.accepted_id);
            res.redirect('/disasterpage_' + disastername);
        }
    });
});



// router.get('/approveacceptedid/:disastername', function (req, res) {
//     var disastername = req.params.disastername;
//     var checkedboxes = req.body.acceptedid;
//     console.log('input from checkboxes', typeof checkedboxes);
//     var selectedcert = [];
//     // for(var eachmember=0;eachmember<checkedboxes.length;eachmember++){
//     //     if(checkedboxes[eachmember].checked){
//     //         selectedcert.push(checkedboxes[i].value);
//     //     }
//     // }
//     // console.log(selectedcert);
// });



router.get('/certpersondetails', function(req,res){
    res.render('certpersondetails');
});

router.get('/certpersondetails/:certid', function(req,res){
    var certpersonid=req.params.certid;
    console.log(certpersonid);
    // $.ajax({
    //     url: 'https://api.mlab.com/api/1/databases/cert/collections/my-coll1/' + certpersonid + '?apiKey=F4GTF2uFESKCFCx7q87ZtFmrhtL-A1-j',
    //     method: 'get',
    //     dataType: 'json',
    //     success: function (data1) {
    //        console.log(data1);
    //     }

    // })
    res.render('certpersondetails',{
        certidis: certpersonid
    });
    // res.redirect('/certpersondetails');
});
router.get('/approveacceptedid/:disastername/:dateandtime/:checkedarray', function (req, res) {
    console.log("helloooooooooo i am there");
    var checboxesarray = req.params.checkedarray;
    var disastername = req.params.disastername;
    console.log('disaster name is in updating checked array is: ', disastername);
    var dateandtime = req.params.dateandtime;
    var query = { disastername: disastername, dateandtime: dateandtime };
    var updatedvalues = { approved_id: checboxesarray };
    console.log('checked boxes array is ', checboxesarray);
    disastermodel.findOne({ disastername: disastername, dateandtime: dateandtime }, function (err, docs) {
        var alldata = docs;
        console.log('all data of the disaster', alldata);
        if (err) {
            res.render('disasterpage', {
                messagefromdb: "could not find the disaster",
                docs: alldata
            });
        } else {
            console.log('found the disaster and i am ready to edit the disaster');
            var query = { disastername: disastername, dateandtime: dateandtime };
            var updatedvalues = { approved_id: checboxesarray };

            disastermodel.updateOne(query, updatedvalues, function (err, updatedrecord) {

                console.log('just finished the query to update data');
                if (err) {
                    console.log('error in updating the record');
                    res.render('disasterpage', {
                        messagefromdb: "failed to udpate the record",
                        docs: alldata
                    });
                } else {

                    disastermodel.findOne({ disastername: disastername }, function (err, docs) {
                        if (err) {
                            console.log('issue while retrieving');
                            res.render('disasterpage', {
                                messagefromdb: "failed to get the record",
                                docs
                            });
                        } else {
                            console.log('docs on after updating');
                            console.log('docs on after updating', docs);
                            var tourl = '/disasterpage_' + disastername;
                            res.redirect(tourl);
                        }
                    });
                }
            });
        }
    });
});

// }
module.exports = router;




// module.exports = 
function CreateTableFromJSON(inputarray) {
    console.log(inputarray);
    console.log("call to the function create table from json");
    var alldisasters = inputarray;
    var col = [];
    for (var i = 0; i < alldisasters.length; i++) {
        for (var key in alldisasters[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }
    var colhead = document.getElementById("tablehead").innerHTML;
    for (var headdata = 0; headdata < col.length; headdata++) {
        $("#tablehead").append("<th>" + col[headdata] + "</th>");
    }
    for (var i = 0; i < alldisasters.length; i++) {
        var eachrowdata = "<tr>";
        for (var j = 0; j < col.length; j++) {
            var eachrowdata = eachrowdata + "<td>" + myBooks[i][col[j]] + "</td>";
        }
        eachrowdata = eachrowdata + "</tr>"
        $("#rowdata").append(eachrowdata);
    }
}

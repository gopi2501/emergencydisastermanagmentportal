var mongoose = require('mongoose');

var DisasterSchema = mongoose.Schema({
    disasteraddress:{
        type:String,
        required:true
    },
    disastercity:{
        type:String,
        required:true
    },
    disastername:{
        type:String,
        required:true
    },
    disasterdropdown: {
        type: String
    },
    contactperson: {
        type: String
    },
    location: {
        type: String
    },
    latitude: {
        type: String
    },
    longitude:{
        type:String
    },
    dateandtime: {
        type: String
    },
    accepted_id:[],
    state: String,
    zipcode: String,
    numberofimages: Number,
    approved_id:[],
    rejected_id:[]
});

// module.exports=mongoose.model('eocschema', EocSchema);
module.exports = mongoose.model('disasterschema', DisasterSchema);
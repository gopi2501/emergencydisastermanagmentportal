var mongoose = require('mongoose');

var EocSchema = mongoose.Schema({
    firstname: {
        type: String,
        index: true
    },
    lastname: {
        type: String
    },
    homeaddress: {
        address: String,
        city: String,
        state: String,
        zip: String
    },
    officeAddress: {
        address: String,
        city: String,
        state: String,
        zip: String
    },
    title: String,
    phonenumber: {
        mobile: String,
        home: String,
        office: String
    },
    email: String,
    password: String,
    certifications: String
});

module.exports=mongoose.model('eocschema', EocSchema);
// var User = mongoose.model('User', UserSchema);
// module.exports = User;